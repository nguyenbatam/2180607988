# 2180607988

| Title| Check and evaluate students training scores |
| ------ | ------ |
| **Value statement**    |  As a student management staff    |
|        |  I want to check the training points of class members  |
| |so I can update training points for members|
|**Acceptace Criteria**|Given I have a list of training points of all members in that semester|
||And that semester ends|
||When I click check button|
||Then I can check whether a member's training points match the list or not|
||And I can edit points|
|**Definition of done**|Unit Tests Passed|
||Acceptace Criteria Met|
||Code Reviewed|
||Functional Tests Passed|
||Non-Functional Requirement Met|
||Product Owner Accepts User Story|
|**Owner**|Nguyen Ba Tam|
|***Interation**|Unscheduled|
|**Estimate**|5 points|

![Screenshot_2023-10-10_224514](https://gitlab.com/nguyenbatam/2180607988/uploads/684477f32cf4ec8e4658b5a8286fe7d8/Screenshot_2023-10-24_233613.png)

## Test Case

|Test Case ID|Test Ojective |Precondition|Steps:|Test Data|Expected result|
|:-----------|:-------------|:-----------|:-----|:--------|:--------------|
|TC1|Managers can view the list of training points |User is logged into the system|1.Open the application  2. Enter the student code to search  3. Chose Check button|Check for student training points|Users can view the list of training points, then edit and saves the edited data to the database|



# 2180607742

| Title | View Academic Results and Grades |
|:------|:--------------------------|
| Value Statement: | As a student|
||I want the ability to view my grades to have a basic understanding of my academic performance 
|Acceptance Criteria: | Given I am viewing the grades for a specific course,
||When I look at the individual grades for an assignment or assessment,
||Then I see the grades displayed as either numerical scores or letter grades, along with detailed descriptions of each grading component 
|| End I can view my overall grade point average (GPA) if available
|Definition of Done:  | Unit Tests Passed
|| Acceptance Criteria Met
|| Code Reviewed
|| Functional Tests Passed
|| Non-Functional Requirement Met
|| Product Owner Accepts User Story
|Owner:| Mr.Minh
|Iteration: | Unscheduled
|Estimate: | 10 points

![FormDiem](https://i.ibb.co/Wktbd4w/FormDiem.png)
=======
# 2180608143

| Title | student manager staff login an account |
|:------|:--------------------------------|
|**value statement** |As a student manager staff, <br>I want  to log in to an account, <br>so I can access the student management program|
| **Acceptance Criteria**| **Acceptance Criterion 1 :**<br> Given that the account has been register.<br> When a student manager staff click the login or enter button.<br>Then They account will be checked on the school's server.<br> AND They will be allowed to access the student management software.|
||**Acceptance Criterion 2 :**<br> Given that the account has not registered <br> When a student manager staff click the login or enter button. <br> Then They account will be checked on the school's server <br> And ensure the rejection message is displayed under login button.<br> AND They will be not allowed to access the student management software.|
||**Acceptance Criterion 3 :**<br> Given that the account has been register.<br> And They forgot their login password.<br> When a student manager staff click the textlink "forgot the password?" under password text.<br>Then They will be redirected to the account support center page.|  
|**Definition of Done**|- Acceptance Criteria Met<br> - Test Cases Passed<br> - Package Into Specific Folders<br> - Synchronize Folders Between Devices<br> - Show Topic List<br> - Code Reviewed</br> - Product Owner Accepts User Story|
|**owner**| Nguyen Nhat Trinh owner|
|**Iteration**|Unscheluled|
|**Estiminate**|5Points|

![picture](https://gitlab.com/nhattrinhnguyen1601/2180608143/uploads/236a8dbdc089171571de152664714222/z4789828944581_21036ede38f519ba5d54b626ecf85c13.jpg)



=======
| Title | Lecturer update students's score into score sheet |
|:------|:--------------------------|
| Value Statement | As a Lecturer<br>I want to know my classes and my students's information that I'm teaching <br> So that I can update my students's score into the score sheet |
|Acceptance Criteria: | Given I have my classes and my students's infomation that I'm teaching in that semester<br><br>When I type in my students's score for each class <br>And I click the update button<br><br>Then I will have my students's component scores and final scores of each class saved successfully in the Database
|Definition of Done:  | Unit Tests Passed<br>Acceptance Criteria Met<br>Code Reviewed<br>Functional Tests Passed<br>Non-Functional Requirement Met<br>Product Owner Accepts User Story
Owner:| Mr.Trung
|Iteration: | Unscheduled
|Estimate: | 10 points|

![Students_s_score_sheet](https://gitlab.com/2180608149/2180608149/uploads/ed7e12d15f1dfff6ed5afa241924b084/Students_s_score_sheet.png)



=======
# thuy linh
| TiTel               | Update personal information                                                       |
| ------------------- | --------------------------------------------------------------------------------- |
| **value statement** | As a student, I would like to update my personal information                      |
| -----               | --------------------------                                                        |
| Acceptance Criteria | -Acceptance criterion 1:                                                          |
|                     | given that update is true information                                             |
|                     | -when i enter the information                                                     |
|                     | -and click the confirm information button                                         |
|                     | -and check again to make sure it is correct                                       |
|                     | -then i have successfully update the information.                                 |
|                     | -Acceptance criterion 2:                                                          |
|                     | given that my information is not update                                           |
|                     | - when i enter the information,i didn't fill in the correct order and characters. |
|                     | -and click the confirm information button                                         |
|                     | -then got an error                                                                |
| Definition Of Done  | - Unit tests passed                                                               |
|                     | - Students please change your personal information                                |
|                     | - Student information changes are accepted                                        |
| Owner               | Vu Thuy Linh                                                                      |
| Iteration           | Unschedules                                                                       |
| Estimate            | 5 Points                                                                          |

![CHANGE](https://gitlab.com/thuy-linh/thuy-linh/uploads/7cb562d8f353e3f68ed465b166d84d93/CHANGE.png)

